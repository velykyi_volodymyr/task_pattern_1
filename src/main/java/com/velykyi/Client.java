package com.velykyi;

import com.velykyi.bakeries.Bakery;
import com.velykyi.bakeries.impl.Dnipro;
import com.velykyi.bakeries.impl.Kyiv;
import com.velykyi.bakeries.impl.Lviv;

import java.util.*;

public class Client {
    private static Scanner scann = new Scanner(System.in);
    private static Map<String, String> bakeryList;
    private static Map<String, Printable> bakeryMethod;
    private static Map<String, String> pizzaList;
    private static Map<String, Printable> pizzaMethod;
    private Map<String, Printable> methodMenu;
    private Bakery bakery;

    public static void main(String[] args) {
        Client client = new Client();
        client.showMenu(bakeryList);
    }

    public Client() {
        this.setBakeryList();
        this.setPizzaList();
        this.methodMenu = bakeryMethod;
    }

    private void setBakeryList() {
        bakeryList = new LinkedHashMap<>();
        bakeryList.put("1", "1 - Lviv;");
        bakeryList.put("2", "3 - Kyiv;");
        bakeryList.put("3", "4 - Dnipro;");
        bakeryList.put("0", "0 - Exit.");

        bakeryMethod = new LinkedHashMap<>();
        bakeryMethod.put("1", this::getLvivPizza);
        bakeryMethod.put("2", this::getKyivPizza);
        bakeryMethod.put("3", this::getDniproPizza);
    }

    private void setPizzaList() {
        pizzaList = new LinkedHashMap<>();
        pizzaList.put("1", "1 - Cheese recipe;");
        pizzaList.put("2", "2 - Cheese bake;");
        pizzaList.put("3", "3 - Clam recipe;");
        pizzaList.put("4", "4 - Clam bake;");
        pizzaList.put("5", "5 - Pepperoni recipe;");
        pizzaList.put("6", "6 - Pepperoni bake;");
        pizzaList.put("7", "7 - Veggie recipe;");
        pizzaList.put("8", "8 - Veggie bake;");
        pizzaList.put("0", "0 - Back.");

        pizzaMethod = new LinkedHashMap<>();
        pizzaMethod.put("1", this::getCheeseRecipe);
        pizzaMethod.put("2", this::getCheese);
        pizzaMethod.put("3", this::getClamRecipe);
        pizzaMethod.put("4", this::getClam);
        pizzaMethod.put("5", this::getPepperoniRecipe);
        pizzaMethod.put("6", this::getPepperoni);
        pizzaMethod.put("7", this::getVeggieRecipe);
        pizzaMethod.put("8", this::getVeggie);
    }

    private void getDniproPizza() {
        bakery = new Dnipro();
        methodMenu = pizzaMethod;
        showMenu(pizzaList);
    }

    private void getKyivPizza() {
        bakery = new Kyiv();
        methodMenu = pizzaMethod;
        showMenu(pizzaList);
    }

    private void getLvivPizza() {
        bakery = new Lviv();
        methodMenu = pizzaMethod;
        showMenu(pizzaList);
    }

    private void getVeggieRecipe() {
        bakery.getRecipe(PizzaType.Veggie);
    }

    private void getPepperoniRecipe() {
        bakery.getRecipe(PizzaType.Pepperoni);
    }

    private void getClamRecipe() {
        bakery.getRecipe(PizzaType.Clam);
    }

    private void getCheeseRecipe() {
        bakery.getRecipe(PizzaType.Cheese);
    }

    private void getVeggie() {
        bakery.assemble(PizzaType.Veggie);
    }

    private void getPepperoni() {
        bakery.assemble(PizzaType.Pepperoni);
    }

    private void getClam() {
        bakery.assemble(PizzaType.Clam);
    }

    private void getCheese() {
        bakery.assemble(PizzaType.Cheese);
    }

    private void outputMenu(Map<String, String> menu){
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    private void showMenu(Map<String, String> menu) {
        String keyMenu;
        do {
            outputMenu(menu);
            System.out.println("Please, select menu point.");
            keyMenu = scann.nextLine().toUpperCase();
            try {
                methodMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("0"));
    }
}
