package com.velykyi;

public enum PizzaType {
    Cheese, Veggie, Clam, Pepperoni
}
