package com.velykyi.bakeries;

import com.velykyi.PizzaType;
import com.velykyi.pizza.Pizza;

public abstract class Bakery {
    protected abstract Pizza bakePizza(PizzaType pizzaType);

    public abstract void getRecipe(PizzaType pizzaType);

    public void assemble(PizzaType pizzaType) {
        Pizza pizza = bakePizza(pizzaType);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
    }
}
