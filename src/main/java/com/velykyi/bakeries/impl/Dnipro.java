package com.velykyi.bakeries.impl;

import com.velykyi.PizzaType;
import com.velykyi.bakeries.Bakery;
import com.velykyi.components.Components;
import com.velykyi.components.impl.*;
import com.velykyi.pizza.Pizza;
import com.velykyi.pizza.impl.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Dnipro extends Bakery {
    private static Logger logger1 = LogManager.getLogger(Lviv.class);

    @Override
    protected Pizza bakePizza(PizzaType pizzaType) {
        Pizza pizza = null;
        if (pizzaType == PizzaType.Cheese) {
            pizza = new Cheese(new DniproCheese());
        } else if (pizzaType == PizzaType.Clam) {
            pizza = new Clam(new DniproClam());
        } else if (pizzaType == PizzaType.Pepperoni) {
            pizza = new Pepperoni(new ComponentsPepperoni());
        } else if (pizzaType == PizzaType.Veggie) {
            pizza = new Veggie(new DniproVeggie());
        }
        return pizza;
    }

    @Override
    public void getRecipe(PizzaType pizzaType) {
        Components pizza = null;
        if (pizzaType == PizzaType.Cheese) {
            pizza = new DniproCheese();
            pizza.getRecipe();
        } else if (pizzaType == PizzaType.Clam) {
            pizza = new DniproClam();
            pizza.getRecipe();
        } else if (pizzaType == PizzaType.Pepperoni) {
            pizza = new ComponentsPepperoni();
            pizza.getRecipe();
        } else if (pizzaType == PizzaType.Veggie) {
            pizza = new DniproVeggie();
            pizza.getRecipe();
        }
    }
}
