package com.velykyi.bakeries.impl;

import com.velykyi.PizzaType;
import com.velykyi.bakeries.Bakery;
import com.velykyi.components.Components;
import com.velykyi.components.impl.*;
import com.velykyi.pizza.Pizza;
import com.velykyi.pizza.impl.Cheese;
import com.velykyi.pizza.impl.Clam;
import com.velykyi.pizza.impl.Pepperoni;
import com.velykyi.pizza.impl.Veggie;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Kyiv extends Bakery {
    private static Logger logger1 = LogManager.getLogger(Lviv.class);

    @Override
    protected Pizza bakePizza(PizzaType pizzaType) {
        Pizza pizza = null;
        if (pizzaType == PizzaType.Cheese) {
            pizza = new Cheese(new KyivCheese());
        } else if (pizzaType == PizzaType.Clam) {
            pizza = new Clam(new KyivClam());
        } else if (pizzaType == PizzaType.Pepperoni) {
            pizza = new Pepperoni(new ComponentsPepperoni());
        } else if (pizzaType == PizzaType.Veggie) {
            pizza = new Veggie(new KyivVeggie());
        }
        return pizza;
    }

    @Override
    public void getRecipe(PizzaType pizzaType) {
        Components pizza = null;
        if (pizzaType == PizzaType.Cheese) {
            pizza = new KyivCheese();
            pizza.getRecipe();
        } else if (pizzaType == PizzaType.Clam) {
            pizza = new KyivClam();
            pizza.getRecipe();
        } else if (pizzaType == PizzaType.Pepperoni) {
            pizza = new ComponentsPepperoni();
            pizza.getRecipe();
        } else if (pizzaType == PizzaType.Veggie) {
            pizza = new KyivVeggie();
            pizza.getRecipe();
        }
    }
}
