package com.velykyi.components.impl;

import com.velykyi.components.Components;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ComponentsPepperoni implements Components {
    private static Logger logger1 = LogManager.getLogger(LvivCheese.class);
    private  final String COMPONENTS = "parmesan cheese, oregano," +
            " part-skim mozzarella cheese and pepperoni";

    public String getCOMPONENTS() {
        return COMPONENTS;
    }

    @Override
    public void getRecipe() {
        logger1.info("Pepperoni: " + COMPONENTS);
    }
}
