package com.velykyi.components.impl;

import com.velykyi.components.Components;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DniproVeggie implements Components {
    private static Logger logger1 = LogManager.getLogger(LvivCheese.class);
    private final String COMPONENTS = "olive oil, zucchini, mushrooms, red pepper," +
            " onion and part-skim mozzarella cheese";

    public String getCOMPONENTS() {
        return COMPONENTS;
    }

    @Override
    public void getRecipe() {
        logger1.info("Veggie: " + COMPONENTS);
    }
}
