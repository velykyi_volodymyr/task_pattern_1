package com.velykyi.components.impl;

import com.velykyi.components.Components;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class KyivClam implements Components {
    private static Logger logger1 = LogManager.getLogger(LvivCheese.class);
    private final String COMPONENTS = "fresh clams, olive oil, oregano, garlic," +
            " mozzarella, parmesan, fresh parsley and red pepper";

    public String getCOMPONENTS() {
        return COMPONENTS;
    }

    @Override
    public void getRecipe() {
        logger1.info("Clam: " + COMPONENTS);
    }
}
