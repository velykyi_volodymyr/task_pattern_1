package com.velykyi.components.impl;

import com.velykyi.components.Components;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class KyivVeggie implements Components {
    private static Logger logger1 = LogManager.getLogger(LvivCheese.class);
    private final String COMPONENTS = "cream cheese, sour cream, dill weed, garlic," +
            " fresh broccoli florets, cucumber, tomato and carrot";

    public String getCOMPONENTS() {
        return COMPONENTS;
    }

    @Override
    public void getRecipe() {
        logger1.info("Veggie: " + COMPONENTS);
    }
}
