package com.velykyi.pizza.impl;

import com.velykyi.components.Components;
import com.velykyi.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Clam implements Pizza {
    private static Logger logger1 = LogManager.getLogger(Cheese.class);
    private Components components;

    public Clam(Components components) {
        this.components = components;
    }

    public void prepare() {
        logger1.info(this.components.getCOMPONENTS() + " for clam pizza is preparing.");
    }

    public void bake() {
        logger1.info(this.components + " is baking.");
    }

    public void cut() {
        logger1.info("Clam pizza is cutting.");
    }

    public void box() {
        logger1.info("Clam pizza is boxing.");
    }
}
